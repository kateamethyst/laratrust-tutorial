<?php

namespace App;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    /**
     * Table related to the model
     * @var string
     */
    protected $table = 'roles';

    /**
     * Mass assignable fields to model
     * @var [type]
     */
    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];
}