<?php

namespace App\Http\Controllers;

use App\User;
Use App\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $users =  $user->with('roles')->get();
        return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user, Role $role, Request $request)
    {
        $params = array(
            'email'    => $request->email,
            'password' => bcrypt($request->password),
            'name'     => $request->name
        );

        $users = $user->create($params);
        $new_user = $user->find($users->id);
        $roles = $role->where('name', $request->role)->first();
        $users->roles()->attach($roles->id);
        return response()->json($users, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $users = $user->where('id', $user->id)->with('roles')->first();
        return response()->json($users, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Role $role, Request $request)
    {
        $user->roles()->detach();
        $params = array(
            'email'    => $request->email,
            'password' => bcrypt($request->password),
            'name'     => $request->name
        );
        $users = $user->fill($params)->save();
        $users = $user->find($user->id);
        $roles = $role->where('name', $request->role)->first();
        $users->roles()->attach($roles->id);
        $users = $user->where('id', $user->id)->with('roles')->first();
        return response()->json($users, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $users = $user->delete($user->id);
        return response()->json($users, 200);
    }
}
