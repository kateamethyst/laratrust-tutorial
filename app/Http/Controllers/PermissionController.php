<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\Role;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Permission $permission)
    {
        $permissions = $permission->all();
        return response()->json($permissions, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Permission $permission, Request $request)
    {
        $params = $request->only('name', 'display_name', 'description');
        $permissions = $permission->create($params);
        return response()->json($permissions, 200);
    }

    public function attachPermission(Role $role, Permission $permission, Request $request)
    {
        $roles = $role->find($request->role);
        $permissions = $permission->find($request->permission);

        // equivalent to $admin->permissions()->attach([$createPost->id]);
        $roles->attachPermissions($permissions);
        return response()->json(['message' => 'attached'], 200);
    }

    public function removePermission(Role $role, Permission $permission, Request $request)
    {
        $roles = $role->find($request->role);
        $permissions = $permission->find($request->permission);

        // equivalent to $admin->permissions()->detach([$createPost->id]);
        $roles->detachPermission($permissions);
        return response()->json(['message' => 'remove'], 200);
    }
}
